#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import argparse
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser(add_help=False)
	args.add_argument('--workroom',			default='workroom',	type=str)		# general setting
	args.add_argument('--data_dir',			default='',			type=str)		# general setting
	args.add_argument('--gpu',				default=-1,			type=int)		# dummy
	args = vars(args.parse_args())
	return namedtuple('_',args.keys())(**args)	# set all settings immutable
	
# -------------------------------------------------------------------
import os, json, pickle
from collections import defaultdict
def main():
	config = get_config()

	counts = defaultdict(int)
	for d in os.listdir(config.data_dir):
		if '.DS' in d: continue

		file_name = f'{config.data_dir}/{d}/config.json'
		with open(file_name, 'r') as fp:
			c = json.load(fp)
		data = c['dataset']

		file_name = f'{config.data_dir}/{d}/trails.pkl'
		with open(file_name, 'rb') as fp:
			trails = pickle.load(fp)

		for s1,s2 in zip(trails[:-1], trails[1:]):
			counts[data] += s1['train']['corrects'] & (~s2['train']['corrects'])

	os.makedirs(config.workroom, exist_ok=True)
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	for data in counts:
		with open(f'{config.workroom}/{data}.pkl', 'wb') as fp:
			pickle.dump(counts[data], fp)
	
	report('finish')

# -------------------------------------------------------------------
if __name__=='__main__':
	main()


