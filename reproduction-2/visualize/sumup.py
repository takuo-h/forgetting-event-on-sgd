#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib import pyplot as plt
import numpy
def draw(buckets, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(5,3.5))
	plt.subplots_adjust(left=0.2, right=0.97, top=0.96, bottom=0.15)

	ax = fig.add_subplot(1,1,1)

	x = sorted(buckets.keys())
	y = [buckets[v] for v in x]

	ax.plot(x,y, lw=0.8, color='black', zorder=1)
	ax.scatter(x,y,s=30,marker='+',zorder=2,color='red',linewidths=0.5)
	
	ax.set_xlabel('number of removing instance')
	ax.set_ylabel('miss-classification rate')
		
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)


# -------------------------------------------------------------------
import os, json, pickle
from collections import defaultdict
def load_results():
	buckets = defaultdict(list)
	#  "dataset": "mnist",
	#  "K": 0

	archive = 'MIRROR/ARCHIVEs/EVALUATIONs/'
	if not os.path.exists(archive): return
	for d in os.listdir(archive):
		if '.DS' in d: continue

		file_name = f'{archive}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)
		data = config['dataset']
		K 	 = config['K']


		file_name = f'{archive}/{d}/trails.pkl'
		with open(file_name, 'rb') as fp:
			trails = pickle.load(fp)

		buckets[data,K].append(trails[-1]['test']['accuracy'])

	for key in buckets:
		buckets[key] = sum(buckets[key])/len(buckets[key])

	accuracies = defaultdict(dict)
	for data,K in buckets:
		accuracies[data][K] = buckets[data,K]

	for data in accuracies:
		yield data, accuracies[data]

# -------------------------------------------------------------------
def main():
	for data,buckets in load_results():
		output_file = f"VIEW/sumup/data={data}.png"
		draw(buckets, output_file)

	report('finish')

# -------------------------------------------------------------------
if __name__=='__main__':
	main()


