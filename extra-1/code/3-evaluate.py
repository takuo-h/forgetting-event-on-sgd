#!/usr/bin/env python
# coding: utf-8

import torch
torch.backends.cudnn.benchmark = True

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import copy
import torch.nn.functional as F
def run_under_removal(datasets, model, config):
	init_param = torch.load(f'{config.episode}/params/{config.start_epoch:02d}.pt')

	def sgd_with_skip(removal_index):
		model.set_param(copy.deepcopy(init_param), gpu=config.gpu)
		optimizer = torch.optim.SGD(model.parameters(), lr=config.lr, momentum=config.momentum)
		for epoch in range(config.start_epoch, config.num_epochs):
			model.train()
			for i,x,y in datasets.train:
				wrong_mask = torch.tensor([iii.item() in removal_index for iii in i])
				good_mask  = ~wrong_mask
				good_mask  = good_mask.cuda(config.gpu)
								
				x = x[good_mask]
				y = y[good_mask]

				logit = model(x)
				loss  = F.cross_entropy(logit, y)
				
				optimizer.zero_grad()
				loss.backward()
				optimizer.step()
		measure = {}
		measure['test']	 = commons.eval_model(model, datasets.test)
		return measure

	# remove examples
	with open(f'{config.score_dir}/{config.dataset}.pkl','rb') as fp:
		scores = pickle.load(fp)

	priority = {}
	priority['events']	= np.argsort(-scores)
	np.random.seed(config.seed)
	priority['random']	= np.random.permutation(datasets.train.n)
	
	Ks = [1, 3, 6, 10, 30, 60, 100, 300, 600, 1000, 3000, 6000, 10000]
	
	evaluations = {}
	for method in priority:
		for k in Ks:
			report(f'method:{method} k:{k}')
			removal_index = set(priority[method][:k])
			evaluations[method, k] = sgd_with_skip(removal_index)
	report(f'method:baseline')
	removal_index = []
	evaluations['baseline'] = sgd_with_skip(removal_index)
	return evaluations


# -------------------------------------------------------------------
import argparse, time
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser(add_help=False)
	args.add_argument('--workroom',			default='workroom',	type=str)		# general setting
	args.add_argument('--episode',			default='workroom',	type=str)		# general setting
	args.add_argument('--gpu',				default=0,			type=int)		# general setting
	# removal settings
	args.add_argument('--start_epoch',		default=19,			type=int)
	args.add_argument('--score_dir',		default='',			type=str)
	# 
	args = args.parse_args()
	with open(f'{args.episode}/config.json','r') as fp:
		config = json.load(fp)
	args = config | vars(args)	# carry over dataset, seed, nval, test_batch_size, lr, momentum, batch_size, num_epochs
	return namedtuple('_',args.keys())(**args)	# set all settings immutable
	
# -------------------------------------------------------------------
import commons
import json, pickle, os
import numpy as np
def main():
	print('-'*50)
	report('start')

	config      = get_config()
	datasets	= commons.get_dataset(config)
	model		= commons.get_model(config)
	evaluations	= run_under_removal(datasets, model, config)

	os.makedirs(config.workroom, exist_ok=True)
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	with open(f'{config.workroom}/trails.pkl', 'wb') as fp:
		pickle.dump(evaluations, fp)
	
	report('finished')
	print('-'*50)
	
if __name__ == '__main__':
	main()
