#!/usr/bin/env python
# coding: utf-8

import torch
torch.backends.cudnn.benchmark = True

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import torch.nn.functional as F
def train(datasets, model, config):
	optimizer = torch.optim.SGD(model.parameters(), lr=config.lr, momentum=config.momentum)

	def update(dataset, epoch):
		os.makedirs(f'{config.workroom}/params', exist_ok=True)
		torch.save(model.get_param(), f'{config.workroom}/params/{epoch:02d}.pt')
		measure = {'loss':0, 'corrects':(torch.ones(dataset.n)==1)}
		model.train()
		for i,x,y in dataset:
			logit = model(x)
			loss  = F.cross_entropy(logit, y)

			optimizer.zero_grad()
			loss.backward()
			optimizer.step()
			
			measure['loss']			+= loss.item() * len(x)
			measure['corrects'][i]   = (logit.argmax(dim=1)==y).cpu()
		measure['loss'] 	/= dataset.n
		measure['accuracy']  = measure['corrects'].float().mean()
		measure['corrects']	 = measure['corrects'].numpy()
		return measure

	trails = []
	for epoch in range(config.num_epochs):
		status = {}
		status['train']	= update(datasets.train, epoch)
		status['test']	= commons.eval_model( model, datasets.test)
		trails.append(status)
		
		report(f'epoch:{epoch}/{config.num_epochs}')
		for key in ['train','test']:
			message	 = [f'\t{key:6}']
			message	+= [f"loss:{status[key]['loss']: 15.7f}"]
			message	+= [f"accuracy:{status[key]['accuracy']: 9.7f}"]
			report(*message)
	return trails	

# -------------------------------------------------------------------
import argparse, time
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser(add_help=False)
	args.add_argument('--workroom',			default='workroom',	type=str)		# general setting
	args.add_argument('--dataset',			default='mnist',	type=str,	choices=['mnist','cifar10'])
	args.add_argument('--seed',				default=0,			type=lambda x: int(x) if x.isdecimal() else int(time.time()) )
	args.add_argument('--gpu',				default=0,			type=int)		# general setting
	args.add_argument('--batch_size',		default=64,			type=int)		# training
	args.add_argument('--lr',				default=0.05,		type=float)		# .
	args.add_argument('--momentum',			default=0.0,		type=float)		# .
	args.add_argument('--num_epochs',		default=3,			type=int)		# training
	args.add_argument('--test_batch_size',	default=10000,		type=int)		# evaluation
	args = vars(args.parse_args())
	return namedtuple('_',args.keys())(**args)	# set all settings immutable
	
# -------------------------------------------------------------------
import commons
import json, pickle, os
def main():
	print('-'*50)
	report('start')

	config      = get_config()
	datasets	= commons.get_dataset(config)
	model		= commons.get_model(config)
	trails 		= train(datasets, model, config)

	os.makedirs(config.workroom, exist_ok=True)
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	with open(f'{config.workroom}/trails.pkl', 'wb') as fp:
		pickle.dump(trails, fp)

	report('finished')
	print('-'*50)
	
if __name__ == '__main__':
	main()
