 #!/usr/bin/env python
# coding: utf-8

# pip install -r experiment/requirements.txt

# ------------------------------------------------------
import os
from os import path
import query_queue_and_parallel as qqp
def main():
	available_gpu	= list(range(5))
	num_of_trial	= 5

	query = qqp.Query(archive='ARCHIVEs/TRAINs')
	query['scripts']		= [path.join(path.dirname(__file__),'code/1-train.py')]
	query['dataset']		= ['mnist','cifar10']
	query['seed']			= list(range(num_of_trial))
	query['num_epochs']		= [20]
	query['lr']				= [0.05]
	query['momentum']		= [0.0]
	query.ready()
	query.run(GPU_index=available_gpu)
	
	# score
	query = qqp.Query(archive='ARCHIVEs/SCOREs')
	query['scripts']	= [path.join(path.dirname(__file__),'code/2-score.py')]
	query['data_dir']	= ['ARCHIVEs/TRAINs']
	query.ready()
	query.run(GPU_index=[-1])
	
	
	archive = 'ARCHIVEs/TRAINs'
	episodes = [f'{archive}/{e}' for e in os.listdir(archive)]

	archive = 'ARCHIVEs/SCOREs'
	score_dirs = [f'{archive}/{e}' for e in os.listdir(archive)]

	# run counter-factually
	query = qqp.Query(archive='ARCHIVEs/EVALUATIONs')
	query['scripts']		= [path.join(path.dirname(__file__),'code/3-evaluate.py')]
	query['start_epoch']	= [0,19]
	query['score_dir']		= score_dirs
	query['episode']		= episodes
	query.ready()
	query.run(GPU_index=available_gpu)
	
if __name__ == '__main__':
	main()
