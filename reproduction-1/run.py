 #!/usr/bin/env python
# coding: utf-8

# pip install -r experiment/requirements.txt

# ------------------------------------------------------
from os import path
import query_queue_and_parallel as qqp
def main():
	available_gpu	= list(range(8))
	num_of_trial	= 16

	query = qqp.Query(archive='ARCHIVEs')
	query['scripts']		= [path.join(path.dirname(__file__),'code/train.py')]
	query['dataset']		= ['mnist','cifar10']
	query['seed']			= list(range(num_of_trial))
	query['num_epochs']		= [200]
	# lr and momentum follow hyper-parameters used in public cdes
	query['lr']				= [0.01]
	query['momentum']		= [0.5]
	query.ready()
	query.run(GPU_index=available_gpu)
	
if __name__ == '__main__':
	main()
