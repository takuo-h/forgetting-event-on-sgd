#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib import pyplot as plt
import numpy
def draw(freq, file_name, mode):	
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(5,3.5))
	plt.subplots_adjust(left=0.2, right=0.97, top=0.96, bottom=0.15)

	ax = fig.add_subplot(1,1,1)

	keys = freq.keys()
	x = range(min(keys), max(keys)+1)
	y = [freq[v] for v in x]

	ax.plot(x,y, lw=0.3, color='black', zorder=1)
	ax.scatter(x,y,s=30,marker='+',zorder=2,color='red',linewidths=0.5)
	ax.set_xlabel('number of forgetting event')
	ax.set_ylabel('number of instance\nhaving the number of event')
	
	if mode=='log':
		ax.set_xscale('log')
		ax.set_yscale('log')
		ax.minorticks_off()

		ticks = [10**v for v in [0,1,2,3,4]]
		ticks = list(map(int,ticks))
		for v in ticks:
			ax.axhline(y=v, color='black', lw=0.3, linestyle='--', zorder=-1, alpha=0.5)
			ax.axvline(x=v, color='black', lw=0.3, linestyle='--', zorder=-1, alpha=0.5)
		ax.set_xticks(ticks)
		ax.set_yticks(ticks)
		ax.set_xticklabels([f'{v:d}' for v in ticks])
		ax.set_yticklabels([f'{v:d}' for v in ticks])

		ax.set_xlim(0.5, max(x)*1.2)
		ax.set_ylim(0.5, max(y)*1.2)

	if mode=='small':
		ax.set_xlim(-1, 20)
		xticks = range(0,21,5)
		ax.set_xticks(xticks)
		for x in xticks:
			ax.axvline(x=x, color='black', lw=0.3, linestyle='--', zorder=-1, alpha=0.5)
			
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)


# -------------------------------------------------------------------
import os, json, pickle
from collections import defaultdict
def load_results():
	buckets = defaultdict(int)

	archive = 'MIRROR/ARCHIVEs/'
	if not os.path.exists(archive): return
	for d in os.listdir(archive):
		if '.DS' in d: continue

		file_name = f'{archive}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)
		data = config['dataset']

		file_name = f'{archive}/{d}/trails.pkl'
		with open(file_name, 'rb') as fp:
			trails = pickle.load(fp)

		for s1,s2 in zip(trails[:-1], trails[1:]):
			buckets[data] += s1['train']['corrects'] & (~s2['train']['corrects'])

	freq = {}
	for data in buckets:
		freq[data] = defaultdict(int)
		for v in buckets[data]:
			freq[data][v] += 1

	for data in freq:
		yield data, freq[data]

# -------------------------------------------------------------------
def main():
	for data,freq in load_results():
		for mode in ['small','full','log']:
			output_file = f"VIEW/data={data}/total/{mode}.png"
			draw(freq, output_file, mode)

	report('finish')

# -------------------------------------------------------------------
if __name__=='__main__':
	main()


